/******************************************************************************
 * @file            pe.c
 *
 * Released to the public domain.
 *
 * Anyone and anything may copy, edit, publish, use, compile, sell and
 * distribute this work and all its parts in any form for any purpose,
 * commercial and non-commercial, without any restrictions, without
 * complying with any conditions and by any means.
 *****************************************************************************/
#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>
#include    <time.h>

#include    "pe.h"
#include    "ld.h"

static struct msdos_header *doshdr;
static struct pe_header *pehdr;
static struct pe_optional_header *opthdr;

static struct section_table_entry *section_text;
static struct section_table_entry *section_data;

char dos_stub[] = {

    0x0E,   0x1F,   0xBA,   0x0E,   0x00,   0xB4,   0x09,   0xCD,
    0x21,   0xB8,   0x01,   0x4C,   0xCD,   0x21,   0x54,   0x68,
    
    0x69,   0x73,   0x20,   0x70,   0x72,   0x6F,   0x67,   0x72,
    0x61,   0x6D,   0x20,   0x63,   0x61,   0x6E,   0x6E,   0x6F,
    
    0x74,   0x20,   0x62,   0x65,   0x20,   0x72,   0x75,   0x6E,
    0x20,   0x69,   0x6E,   0x20,   0x44,   0x4F,   0x53,   0x20,
    
    0x6D,   0x6F,   0x64,   0x65,   0x2E,   0x0D,   0x0D,   0x0A,
    0x24,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00

};

static unsigned short chksum (unsigned int checksum, void *base, int count) {

    register int sum = 0;
    int *data;
    
    if (count && base != NULL) {
    
        data = (int *) base;
        
        do {
        
            sum = *(unsigned short *) data + checksum;
            data = (int *) ((char *) data + 2);
            
            checksum = (unsigned short) sum + (sum >> 16);
        
        } while (--count);
    
    }
    
    return checksum + (checksum >> 16);

}

static int pe_chksum (void *base, unsigned int size) {

    void *remaining_data;
    int remaining_data_size;
    
    unsigned int pe_header_chksum, file_chksum;
    unsigned int pe_header_size;
    
    pe_header_size = (unsigned long) pehdr - (unsigned long) base + \
        ((unsigned long) &opthdr->Checksum - (unsigned long) pehdr);
    remaining_data_size = (size - pe_header_size - 4) >> 1;
    remaining_data = &opthdr->Subsystem;
    
    pe_header_chksum = chksum (0, base, pe_header_size >> 1);
    file_chksum = chksum (pe_header_chksum, remaining_data, remaining_data_size);
    
    if (size & 1) {
        file_chksum += (unsigned short) *((char *) base + size - 1);
    }
    
    return size + file_chksum;

}

int init_pe_object (void) {

    header_size = sizeof (*doshdr) + sizeof (dos_stub) + sizeof (*pehdr) + sizeof (*opthdr);
    header_size += (sizeof (*section_text) + sizeof (*section_data));
    
    header_size = ALIGN_UP (header_size, FILE_ALIGNMENT);
    output_size = header_size + ALIGN_UP (text_size, FILE_ALIGNMENT) + ALIGN_UP (data_size, FILE_ALIGNMENT);
    
    if ((output = malloc (output_size)) == NULL) {
        return 2;
    }
    
    memset (output, 0, output_size);
    
    doshdr = (struct msdos_header *) output;
    pehdr  = (struct pe_header *) ((char *) doshdr + sizeof (*doshdr) + sizeof (dos_stub));
    opthdr = (struct pe_optional_header *) ((char *) pehdr + sizeof (*pehdr));
    
    section_text = (struct section_table_entry *) ((char *) opthdr + sizeof (*opthdr));
    section_data = (struct section_table_entry *) ((char *) section_text + sizeof (*section_text));
    
    text = (void *) ((char *) output + header_size);
    data = (void *) ((char *) text + ALIGN_UP (text_size, FILE_ALIGNMENT));
    
    return 0;

}

int write_pe_object (FILE *ofp, unsigned int entry) {

    doshdr->e_magic[0] = 'M';
    doshdr->e_magic[1] = 'Z';
    
    doshdr->e_cblp = 0x0090;
    doshdr->e_cp = 0x0003;
    
    doshdr->e_cparhdr = ALIGN_UP (sizeof (*doshdr), 16) / 16;
    
    doshdr->e_maxalloc = 0xFFFF;
    doshdr->e_sp = 0x00B8;
    
    doshdr->e_lfarlc = sizeof (*doshdr);
    doshdr->e_lfanew = sizeof (*doshdr) + sizeof (dos_stub);
    
    memcpy ((char *)  output + doshdr->e_lfarlc, dos_stub, sizeof (dos_stub));
    
    
    pehdr->Signature[0] = 'P';
    pehdr->Signature[1] = 'E';
    
    pehdr->Machine = IMAGE_FILE_MACHINE_I386;
    pehdr->NumberOfSections = 2;
    pehdr->TimeDateStamp = time (0);
    pehdr->SizeOfOptionalHeader = sizeof (*opthdr);
    
    /*pehdr->Characteristics |= IMAGE_FILE_RELOCS_STRIPPED;*/
    pehdr->Characteristics |= IMAGE_FILE_EXECUTABLE_IMAGE;
    pehdr->Characteristics |= IMAGE_FILE_LINE_NUMS_STRIPPED;
    /*pehdr->Characteristics |= IMAGE_FILE_LOCAL_SYMS_STRIPPED;*/
    pehdr->Characteristics |= IMAGE_FILE_32BIT_MACHINE;
    pehdr->Characteristics |= IMAGE_FILE_DEBUG_STRIPPED;
    
    
    opthdr->Magic = IMAGE_FILE_MAGIC_I386;
    opthdr->MajorLinkerVersion = 2;
    opthdr->MinorLinkerVersion = 1;
    opthdr->SizeOfCode = ALIGN_UP (text_size, FILE_ALIGNMENT);
    opthdr->SizeOfInitializedData = ALIGN_UP (data_size, FILE_ALIGNMENT);
    opthdr->SizeOfUninitializedData = ALIGN_UP (bss_size, FILE_ALIGNMENT);
    opthdr->AddressOfEntryPoint = ALIGN_UP (text - output, SECTION_ALIGNMENT) + entry;
    opthdr->BaseOfCode = ALIGN_UP (text - output, SECTION_ALIGNMENT);
    opthdr->BaseOfData = ALIGN_UP (text - output, SECTION_ALIGNMENT) + ALIGN_UP (text_size, SECTION_ALIGNMENT);
    
    opthdr->ImageBase = 0x00400000;
    opthdr->SectionAlignment = SECTION_ALIGNMENT;
    opthdr->FileAlignment = FILE_ALIGNMENT;
    opthdr->MajorOperatingSystemVersion = 4;
    opthdr->MajorImageVersion = 1;
    opthdr->MajorSubsystemVersion = 4;
    
    opthdr->SizeOfImage = opthdr->BaseOfData + ALIGN_UP (opthdr->SizeOfInitializedData, SECTION_ALIGNMENT);
    opthdr->SizeOfHeaders = ALIGN_UP (header_size, FILE_ALIGNMENT);
    opthdr->Checksum = pe_chksum (output, output_size);
    opthdr->Subsystem = 3;
    
    {
    
        unsigned short ibss_addr = (data - output) + data_size;
        unsigned short ibss_size = bss_size;
        
        unsigned short stack_addr = ibss_addr + ibss_size;
        unsigned short stack_size = ALIGN_UP (stack_addr, SECTION_ALIGNMENT);
        
        opthdr->SizeOfStackReserved = SECTION_ALIGNMENT << 9;
        opthdr->SizeOfStackCommit = ALIGN_UP (stack_addr % 16 + stack_size, SECTION_ALIGNMENT);
        opthdr->SizeOfHeapReserved = SECTION_ALIGNMENT << 8;
        opthdr->SizeOfHeapCommit = ALIGN_UP (stack_addr % 16 + stack_size, SECTION_ALIGNMENT);
    
    }
    
    opthdr->NumberOfRvaAndSizes = 16;
    
    
    memcpy (section_text->Name, ".text", 5);
    
    section_text->VirtualSize = text_size;
    section_text->VirtualAddress = opthdr->BaseOfCode;
    section_text->SizeOfRawData = text_size;
    section_text->PointerToRawData = (text - output);
    
    section_text->Characteristics |= IMAGE_SCN_CNT_CODE;
    section_text->Characteristics |= IMAGE_SCN_ALIGN_4BYTES;
    section_text->Characteristics |= IMAGE_SCN_MEM_EXECUTE;
    section_text->Characteristics |= IMAGE_SCN_MEM_READ;
    
    memcpy (section_data->Name, ".data", 5);
    
    section_data->VirtualSize = data_size;
    section_data->VirtualAddress = opthdr->BaseOfData;
    section_data->SizeOfRawData = data_size;
    section_data->PointerToRawData = (data - output);
    
    section_data->Characteristics |= IMAGE_SCN_CNT_INITIALIZED_DATA;
    section_data->Characteristics |= IMAGE_SCN_ALIGN_4BYTES;
    section_data->Characteristics |= IMAGE_SCN_MEM_READ;
    section_data->Characteristics |= IMAGE_SCN_MEM_WRITE;
    
    
    /* write the file */
    if (fwrite ((char *) output, output_size, 1, ofp) != 1) {
    
        if (program_name) {
            fprintf (stderr, "%s: ", program_name);
        }
        
        fprintf (stderr, "error: failed to write data to '%s'\n", state->outfile);
        return 1;
    
    }
    
    return 0;

}
