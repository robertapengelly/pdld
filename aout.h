/******************************************************************************
 * @file            aout.h
 *
 * Released to the public domain.
 *
 * Anyone and anything may copy, edit, publish, use, compile, sell and
 * distribute this work and all its parts in any form for any purpose,
 * commercial and non-commercial, without any restrictions, without
 * complying with any conditions and by any means.
 *****************************************************************************/
#include    <stddef.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>

#define     SEGMENT_SIZE                0x10000UL

struct aout_exec {

    unsigned int a_info;
    unsigned int a_text;
    unsigned int a_data;
    unsigned int a_bss;
    unsigned int a_syms;
    unsigned int a_entry;
    unsigned int a_trsize;
    unsigned int a_drsize;

};

#define     OMAGIC                      0407
#define     NMAGIC                      0410
#define     ZMAGIC                      0413
#define     QMAGIC                      0314

/* Relocation entry. */
struct relocation_info {

    int r_address;
    unsigned int r_symbolnum;

};

/* Symbol table entry. */
struct nlist {

    /*union {
    
        char *n_name;
        int n_strx;
    
    } n_un;*/
    
    int n_strx;
    unsigned char n_type;
    
    char n_other;
    short n_desc;
    
    unsigned int n_value;

};

/* n_type values: */
#define     N_UNDF                      0x00
#define     N_ABS                       0x02
#define     N_TEXT                      0x04
#define     N_DATA                      0x06
#define     N_BSS                       0x08
#define     N_COMM                      0x12
#define     N_FN                        0x1f

#define     N_EXT                       0x01
#define     N_TYPE                      0x1e

/* Next is the string table,
 * starting with 4 bytes length including the field (so minimum is 4). */

#define     N_TXTOFF(e)                 (0x400)
#define     N_TXTADDR(e)                (SEGMENT_SIZE)
/* this formula doesn't work when the text size is exactly 64k */
#define     N_DATADDR(e)                \
    (N_TXTADDR (e) + SEGMENT_SIZE + ((e).a_text & 0xffff0000UL))
#define     N_BSSADDR(e)                (N_DATADDR (e) + (e).a_data)

#define     N_GETMAGIC(exec)            ((exec).a_info & 0xffff)

struct gr {

    int relocations_count;
    int relocations_max;
    
    struct relocation_info *relocations;

};

extern struct gr tgr;
extern struct gr dgr;

struct aout_object {

    const char *filename;
    void *raw;
    
    struct aout_exec *header;
    struct relocation_info *trelocs, *drelocs;
    
    struct nlist *symtab;
    char *strtab;
    
    int symtab_count, trelocs_count, drelocs_count;
    unsigned int text_slide, data_slide, bss_slide;

};

struct nlist *aout_get_symbol (struct aout_object *object, const char *name);

int aout_glue (struct aout_object *object);

void aout_apply_slides (struct aout_object *object);
void aout_paste (struct aout_object *object);
void aout_undf_collect (struct aout_object *object);

void aout_free (struct aout_object *object);
void aout_init_map (struct aout_object *object);
