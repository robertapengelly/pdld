/******************************************************************************
 * @file            ld.h
 *
 * Released to the public domain.
 *
 * Anyone and anything may copy, edit, publish, use, compile, sell and
 * distribute this work and all its parts in any form for any purpose,
 * commercial and non-commercial, without any restrictions, without
 * complying with any conditions and by any means.
 *****************************************************************************/
#include    <stdio.h>

enum {

    LD_FORMAT_I386_AOUT, LD_FORMAT_I386_COFF, LD_FORMAT_I386_MSDOS, LD_FORMAT_I386_PE,
    LD_FORMAT_BINARY, LD_FORMAT_MSDOS

};

struct object {

    void *data;
    
    void    (*apply_slides)     (void *);
    void    (*free)             (void *);
    
    void*   (*get_symbol)       (void *, const char *);
    int     (*glue)             (void *);
    
    void    (*init_map)         (void *);
    void    (*paste)            (void *);
    void    (*undf_collect)     (void *);

};

struct ld_state {

    char **files;
    int nb_files;
    
    char *outfile;
    int data_specified;
    
    int format;
    int impure;
    int nostdlib;
    int strip_all;
    
    char *entry;
    char *mapfile;
    
    struct object **objects;
    int nb_objects;
    
    int text_offset;
    int text_offset_set;
    
    int data_offset;
    int data_offset_set;

};

#define     FILE_ALIGNMENT              512
#define     SECTION_ALIGNMENT           4096

#define     PAGE_SIZE                   4096

#define     DIV_ROUNDUP(a, b)           (((a) + ((b) - 1)) / (b))
#define     ALIGN_UP(x, a)              (DIV_ROUNDUP ((x), (a)) * (a))

extern struct ld_state *state;
extern const char *program_name;

extern unsigned int text_size;
extern unsigned int data_size;
extern unsigned int bss_size;

extern unsigned int output_size;;

extern void *output;
extern void *text;
extern void *data;

extern size_t header_size;

/* aout.c */
int init_aout_object (void);
int write_aout_object (FILE *ofp, unsigned int a_entry);

/* coff.c */
int init_coff_object (void);
int write_coff_object (FILE *ofp, unsigned int entry);

/* ld.c */
void *get_symbol (const char *filename, const char *name, int quiet);

/* initialize.c */
int initialize_object (const char *filename);

/* libld.c */
char *xstrdup (const char *str);

void *xmalloc (unsigned long size);
void *xrealloc (void *ptr, unsigned long size);

void dynarray_add (void *ptab, int *nb_ptr, void *data);

/* map.c */
void add_map_object (const char *filename, unsigned int a_bss, unsigned int a_data, unsigned int a_text);

void add_map_bss_symbol (const char *filename, const char *symname, unsigned int value);
void add_map_data_symbol (const char *filename, const char *symname, unsigned int value);
void add_map_text_symbol (const char *filename, const char *symname, unsigned int value);

void generate_map (void);
void set_map_sections_size (unsigned int text_size, unsigned int data_size, unsigned int bss_size);
void set_map_sections_start (unsigned int text_start, unsigned int data_start, unsigned int bss_start);

/* msdos.c */
int init_msdos_object (void);
int write_msdos_object (FILE *ofp);

/* pe.c */
int init_pe_object (void);
int write_pe_object (FILE *ofp, unsigned int entry);
