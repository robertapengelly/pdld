/******************************************************************************
 * @file            initialize.c
 *
 * Released to the public domain.
 *
 * Anyone and anything may copy, edit, publish, use, compile, sell and
 * distribute this work and all its parts in any form for any purpose,
 * commercial and non-commercial, without any restrictions, without
 * complying with any conditions and by any means.
 *****************************************************************************/
#include    <errno.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>

#include    "aout.h"
#include    "coff.h"
#include    "ar.h"
#include    "ld.h"

struct object_cache {

    void *data;
    size_t size;

};

static struct object_cache **cached_objects;
static int nb_cached_objects;

static unsigned int conv_dec (char *str, int max) {

    unsigned int value = 0;
    
    while (*str != ' ' && max-- > 0) {
    
        value *= 10;
        value += *str++ - '0';
    
    }
    
    return value;

}

static int initialize_aout (void *object, size_t size, const char *filename, int quiet) {

    struct aout_exec *header = object;
    
    struct nlist *symtab;
    struct relocation_info *trelocs, *drelocs;
    
    struct aout_object *data_object;
    struct object *new_object;
    
    char *strtab;
    
    int symtab_count, trelocs_count, drelocs_count;
    unsigned int symtab_off, strtab_off, trelocs_off, drelocs_off;
    
    struct object_cache *cache;
    int object_i;
    
    if (N_GETMAGIC (*header) != OMAGIC) {
    
        if (!quiet) {
        
            if (program_name) {
                fprintf (stderr, "%s: ", program_name);
            }
            
            fprintf (stderr, "error: '%s' is not a valid a.out object file\n", filename);
        
        }
        
        return 1;
    
    }
    
    for (object_i = 0; object_i < nb_cached_objects; object_i++) {
    
        if ((cache = cached_objects[object_i]) == NULL) {
            continue;
        }
        
        if (cache->data == NULL || cache->size != size) {
            continue;
        }
        
        if (memcmp (object, cache->data, size) == 0) {
            return 0;
        }
    
    }
    
    if (state->impure) {
    
        text_size += header->a_text;
        data_size += header->a_data;
        bss_size += header->a_bss;
    
    } else {
    
        
        if (state->format == LD_FORMAT_I386_PE) {
        
            text_size += ALIGN_UP (header->a_text, FILE_ALIGNMENT);
            data_size += ALIGN_UP (header->a_data, FILE_ALIGNMENT);
            bss_size += ALIGN_UP (header->a_bss, FILE_ALIGNMENT);
        
        } else {
        
            text_size += ALIGN_UP (header->a_text, PAGE_SIZE);
            data_size += ALIGN_UP (header->a_data, PAGE_SIZE);
            bss_size += ALIGN_UP (header->a_bss, PAGE_SIZE);
        
        }
    
    }
    
    trelocs_off = sizeof (*header) + header->a_text + header->a_data;
    drelocs_off = trelocs_off + header->a_trsize;
    symtab_off = drelocs_off + header->a_drsize;
    strtab_off = symtab_off + header->a_syms;
    
    trelocs_count = header->a_trsize / sizeof (*trelocs);
    drelocs_count = header->a_drsize / sizeof (*drelocs);
    symtab_count = header->a_syms / sizeof (*symtab);
    
    symtab = (void *) ((char *) object + symtab_off);
    trelocs = (void *) ((char *) object + trelocs_off);
    drelocs = (void *) ((char *) object + drelocs_off);
    strtab = (char *) object + strtab_off;
    
    if ((new_object = malloc (sizeof (*new_object))) == NULL) {
        return 2;
    }
    
    memset (new_object, 0, sizeof (*new_object));
    
    new_object->apply_slides = (void (*) (void *)) aout_apply_slides;
    new_object->free         = (void (*) (void *)) aout_free;
    new_object->init_map     = (void (*) (void *)) aout_init_map;
    new_object->paste        = (void (*) (void *)) aout_paste;
    new_object->undf_collect = (void (*) (void *)) aout_undf_collect;
    
    new_object->glue         = (int (*) (void *)) aout_glue;
    new_object->get_symbol   = (void* (*) (void *, const char *)) aout_get_symbol;
    
    if ((data_object = malloc (sizeof (*data_object))) == NULL) {
        return 2;
    }
    
    memset (data_object, 0, sizeof (*data_object));
    
    data_object->filename = filename;
    data_object->raw = object;
    data_object->header = header;
    data_object->trelocs = trelocs;
    data_object->drelocs = drelocs;
    data_object->symtab = symtab;
    data_object->strtab = strtab;
    data_object->trelocs_count = trelocs_count;
    data_object->drelocs_count = drelocs_count;
    data_object->symtab_count = symtab_count;
    
    new_object->data = data_object;
    dynarray_add (&state->objects, &state->nb_objects, new_object);
    
    if ((cache = malloc (sizeof (*cache))) == NULL) {
        return 2;
    }
    
    cache->data = object;
    cache->size = size;
    
    dynarray_add (&cached_objects, &nb_cached_objects, cache);
    return 0;

}

static int initialize_coff (void *object, const char *filename, int quiet) {

    struct coff_header *header = object;
    
    if (header->Machine != IMAGE_FILE_MACHINE_I386) {
    
        if (!quiet) {
        
            if (program_name) {
                fprintf (stderr, "%s: ", program_name);
            }
            
            fprintf (stderr, "error: '%s' is not a valid coff object file\n", filename);
        
        }
        
        return 1;
    
    }
    
    if (program_name) {
        fprintf (stderr, "%s: ", program_name);
    }
    
    fprintf (stderr, "Currently coff objects aren't supported\n");
    return 2;

}

static int initialize_archive (FILE *ar_file, const char *root_filename) {

    int err = 0, old_errno, i;
    
    char *filename = 0, *path;
    void *object = NULL;
    
    if ((filename = malloc (17)) == NULL) {
        return 2;
    }
    
    if (fseek (ar_file, 8, SEEK_SET) != 0) {
        goto out_perror;
    }
    
    for (;;) {
    
        struct ar_header header;
        unsigned int size, size_aligned;
        
        if (fread (&header, sizeof (header), 1, ar_file) != 1) {
        
            if (feof (ar_file)) {
                break;
            }
            
			goto out_perror;
        
        }
        
        size = conv_dec (header.size, 10);
        size_aligned = (size % 2) ? (size + 1) : size;
        
        if (memcmp (header.name, "__.SYMDEF", 9) == 0) {
        
            fseek (ar_file, size, SEEK_CUR);
            continue;
        
        } else {
        
            memcpy (filename, header.name, 16);
            
            for (i = 0; i < 16; i++) {
            
                if (filename[i] == 0x20) {
                
                    filename[i] = '\0';
                    break;
                
                }
            
            }
        
        }
        
        if ((object = malloc (size_aligned)) == NULL) {
	        goto out_perror;
        }
        
        if (fread (object, size_aligned, 1, ar_file) != 1) {
	        goto out_perror;
        }
        
        if (filename) {
        
            size_t len = strlen (root_filename) + 1 + strlen (filename) + 2;
            
            if ((path = malloc (len)) == NULL) {
                return 2;
            }
            
            memset (path, 0, len);
            sprintf (path, "%s(%s)", root_filename, filename);
            
            if (initialize_aout (object, size, path, 1)) {
                free (object);
            }
        
        } else {
        
            if (initialize_aout (object, size, "", 1)) {
                free (object);
            }
        
        }
        
        object = NULL;
    
    }
    
    goto out;

out_perror:

	err = 1;
    old_errno = errno;
	
	if (program_name) {
		fprintf (stderr, "%s: ", program_name);
	}
	
    errno = old_errno;
	perror ("error");

out:

    if (object != NULL && err != 0) {
        free (object);
    }
    
    return err;

}

int initialize_object (const char *filename) {

    FILE *object_file = NULL;
    int err = 0, old_errno;
    
    void *object = NULL;
    
    char ar_magic[8];
    long object_size;
    
    if ((object_file = fopen (filename, "rb")) == NULL) {
    	goto out_perror;
    }
    
    if (fread (ar_magic, 8, 1, object_file) != 1) {
    	goto out_perror;
    }
    
    if (memcmp (ar_magic, "!<arch>\n", 8) == 0) {
    
        err = initialize_archive (object_file, filename);
        goto out;
    
    }
    
    if (fseek (object_file, 0, SEEK_END) != 0) {
    	goto out_perror;
    }
    
    if ((object_size = ftell (object_file)) == -1) {
    	goto out_perror;
    }
    
    rewind (object_file);
    
    if ((object = malloc (object_size)) == NULL) {
    	goto out_perror;
    }
    
    if (fread (object, object_size, 1, object_file) != 1) {
    	goto out_perror;
    }
    
    if ((err = initialize_aout (object, object_size, filename, 1))) {
        err = initialize_coff (object, filename, 1);    
    }
    
    goto out;

out_perror:

	err = 1;
    old_errno = errno;
	
	if (program_name) {
		fprintf (stderr, "%s: ", program_name);
	}
	
    errno = old_errno;
	perror ("error");

out:

    if (object_file != NULL) {
        fclose (object_file);
    }
    
    if (object != NULL && err != 0) {
        free (object);
    }
    
    return err;

}
