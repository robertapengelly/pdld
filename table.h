/******************************************************************************
 * @file            table.h
 *
 * Released to the public domain.
 *
 * Anyone and anything may copy, edit, publish, use, compile, sell and
 * distribute this work and all its parts in any form for any purpose,
 * commercial and non-commercial, without any restrictions, without
 * complying with any conditions and by any means.
 *****************************************************************************/
#ifndef     _TABLE_H
#define     _TABLE_H

#include    <stddef.h>

struct name {

    const char *chars;
    
    size_t bytes;
    unsigned int hash;

};

struct table_entry {

    struct name *key;
    void *value;

};

struct table {

    struct table_entry *entries;
    int capacity, count, used;

};

struct name *alloc_name (const char *begin, const char *end, int make_copy);

int table_put (struct table *table, struct name *key, void *value);
void *table_get (struct table *table, struct name *name);

#endif      /* _TABLE_H */
