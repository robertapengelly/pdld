/******************************************************************************
 * @file            aout.c
 *
 * Released to the public domain.
 *
 * Anyone and anything may copy, edit, publish, use, compile, sell and
 * distribute this work and all its parts in any form for any purpose,
 * commercial and non-commercial, without any restrictions, without
 * complying with any conditions and by any means.
 *****************************************************************************/
#include    <stddef.h>
#include    <stdio.h>

#include    "aout.h"
#include    "ld.h"

static int objtextsize = 0;
static int objdatasize = 0;
static int objbsssize = 0;

static unsigned int text_ptr = 0;
static unsigned int data_ptr = 0;
static unsigned int bss_ptr = 0;

static struct aout_exec *header;

struct gr tgr = { 0, 64, NULL };
struct gr dgr = { 0, 64, NULL };

static int add_relocation (struct gr *gr, struct relocation_info *r) {

    if (gr->relocations == NULL) {
    
        if ((gr->relocations = malloc (gr->relocations_max * sizeof (*r))) == NULL) {
            return 1;
        }
    
    }
    
    if (gr->relocations_count >= gr->relocations_max) {
    
        void *tmp;
        
        gr->relocations_max *= 2;
        
        if ((tmp = realloc (gr->relocations, gr->relocations_max * sizeof (*r))) == NULL) {
            return 1;
        }
        
        gr->relocations = tmp;
    
    }
    
    gr->relocations[gr->relocations_count] = *r;
    gr->relocations_count++;
    
    return 0;

}

static int relocate (struct aout_object *object, struct relocation_info *r, int offset, int is_data) {

    struct nlist *symbol;
    int result;
    
    int symbolnum = r->r_symbolnum & 0xffffff;
    int pcrel = (r->r_symbolnum & (1 << 24)) >> 24;
    int ext = (r->r_symbolnum & (1 << 27)) >> 27;
    int baserel = (r->r_symbolnum & (1 << 28)) >> 28;
    int jmptable = (r->r_symbolnum & (1 << 29)) >> 29;
    int rel = (r->r_symbolnum & (1 << 30)) >> 30;
    int copy = (r->r_symbolnum & (1 << 31)) >> 31;

    int length = (r->r_symbolnum & (3 << 25)) >> 25;
    
    switch (length) {
    
        case 0:
        
            length = 1;
            break;
        
        case 1:
        
            length = 2;
            break;
        
        case 2:
        
            length = 4;
            break;
    
    }
    
    if ((is_data && pcrel) || baserel || jmptable || rel || copy) {
    
        if (program_name) {
            fprintf (stderr, "%s: ", program_name);
        }
        
        fprintf (stderr, "error: unsupported relocation type\n");
        return 1;
    
    }
    
    symbol = &object->symtab[symbolnum];
    
    if (ext) {
    
        char *symname = object->strtab + symbol->n_strx;
        
        if ((symbol = get_symbol (object->filename, symname, 0)) == NULL) {
            return 1;
        }
    
    }
    
    if (pcrel) {
        result = (int) symbol->n_value - (r->r_address + length);
    } else {
    
        if (!ext || (symbol->n_type & N_TYPE) == N_BSS || (symbol->n_type & N_TYPE) == N_DATA || (symbol->n_type & N_TYPE) == N_TEXT) {
        
            struct relocation_info new_relocation;
            new_relocation.r_address = r->r_address;
            
            if (is_data) {
                new_relocation.r_address -= text_size;
            }
            
            new_relocation.r_symbolnum = r->r_symbolnum & (3 << 25);
            add_relocation (is_data ? &dgr : &tgr, &new_relocation);
        
        }
        
        result = *(int *) ((char *) output + header_size + r->r_address);
        result += offset;
        
        if (ext) {
            result += symbol->n_value;
        } else {
        
            if ((symbolnum == 6) || (symbolnum == 8)) {
            
                result -= object->header->a_text;
                result += text_size;
            
            }
            
            if (symbolnum == 4) {
                result += objtextsize;
            }
            
            if (symbolnum == 6) {
                result += objdatasize;
            }
            
            if (symbolnum == 8) {
            
                result -= object->header->a_data;
                result += data_size;
                result += objbsssize;
            
            }
        
        }
    
    }
    
    memcpy ((char *) output + header_size + r->r_address, &result, length);
    return 0;

}

struct nlist *aout_get_symbol (struct aout_object *object, const char *name) {

    int symbol_i;
    
    for (symbol_i = 0; symbol_i < object->symtab_count; symbol_i++) {
    
        struct nlist *sym = &object->symtab[symbol_i];
        char *symname = object->strtab + sym->n_strx;
        
        if ((sym->n_type & N_EXT) == 0) {
            continue;
        }
        
        if ((sym->n_type & N_TYPE) != N_TEXT && (sym->n_type & N_TYPE) != N_DATA && (sym->n_type & N_TYPE) != N_BSS && (sym->n_type & N_TYPE) != N_ABS) {
            continue;
        }
        
        if (strcmp (symname, name) == 0) {
            return sym;
        }
    
    }
    
    return NULL;

}

int aout_glue (struct aout_object *object) {

    int i, err = 0;
    
    for (i = 0; i < object->trelocs_count; i++) {
    
        if (relocate (object, &object->trelocs[i], state->text_offset, 0)) {
            err = 1;
        }
    
    }
    
    for (i = 0; i < object->drelocs_count; i++) {
    
        if (relocate (object, &object->drelocs[i], state->data_offset, 1)) {
            err = 1;
        }
    
    }
    
    objtextsize += object->header->a_text;
    objdatasize += object->header->a_data;
    objbsssize += object->header->a_bss;
    
    return err;

}

int init_aout_object (void) {

    header_size = sizeof (*header);
    
    if (!state->impure) {
        header_size = ALIGN_UP (header_size, PAGE_SIZE);
    }
    
    output_size = header_size + text_size + data_size;
    
    if ((output = malloc (output_size)) == NULL) {
        return 2;
    }
    
    memset (output, 0, output_size);
    header = output;
    
    text = (void *) ((char *) output + header_size);
    data = (void *) ((char *) text + text_size);
    
    return 0;

}

int write_aout_object (FILE *ofp, unsigned int a_entry) {

    header->a_info = state->impure ? OMAGIC : ZMAGIC;
    header->a_text = text_size;
    header->a_data = data_size;
    header->a_bss = bss_size;
    header->a_entry = a_entry;
    header->a_trsize = tgr.relocations_count * sizeof (struct relocation_info);
    header->a_drsize = dgr.relocations_count * sizeof (struct relocation_info);
    
    if (fwrite ((char *) output, output_size, 1, ofp) != 1) {
    
        if (program_name) {
            fprintf (stderr, "%s: ", program_name);
        }
        
        fprintf (stderr, "error: failed to write data to '%s'\n", state->outfile);
        return 1;
    
    }
    
    if (tgr.relocations_count > 0) {
    
        if (fwrite (tgr.relocations, tgr.relocations_count * sizeof (struct relocation_info), 1, ofp) != 1) {
        
            if (program_name) {
                fprintf (stderr, "%s: ", program_name);
            }
            
            fprintf (stderr, "error: failed to write data to '%s'\n", state->outfile);
            return 1;
        
        }
    
    }
    
    if (dgr.relocations_count > 0) {
    
        if (fwrite (dgr.relocations, dgr.relocations_count * sizeof (struct relocation_info), 1, ofp) != 1) {
        
            if (program_name) {
                fprintf (stderr, "%s: ", program_name);
            }
            
            fprintf (stderr, "error: failed to write data to '%s'\n", state->outfile);
            return 1;
        
        }
    
    }
    
    return 0;

}

void aout_apply_slides (struct aout_object *object) {

    int i;
    
    for (i = 0; i < object->symtab_count; i++) {
    
        struct nlist *symbol = &object->symtab[i];
        unsigned int final_slide = 0;
        
        if ((symbol->n_type & N_TYPE) != N_TEXT && (symbol->n_type & N_TYPE) != N_DATA && (symbol->n_type & N_TYPE) != N_BSS) {
            continue;
        }
        
        switch (symbol->n_type & N_TYPE) {
        
            case N_BSS:
            
                final_slide += text_size;
                final_slide += data_size;
                final_slide += object->bss_slide;
                
                break;
            
            case N_DATA:
            
                final_slide += text_size;
                final_slide += object->data_slide;
                
                break;
            
            case N_TEXT:
            
                final_slide += object->text_slide;
                break;
        
        }
        
        symbol->n_value += final_slide;
        
        switch (symbol->n_type & N_TYPE) {
        
            case N_BSS:
            
                symbol->n_value -= object->header->a_data;
                /* fall through */
            
            case N_DATA:
            
                symbol->n_value -= object->header->a_text;
                break;
        
        }
    
    }
    
    for (i = 0; i < object->trelocs_count; i++) {
    
        struct relocation_info *rel = &object->trelocs[i];
        rel->r_address += object->text_slide;
    
    }
    
    for (i = 0; i < object->drelocs_count; i++) {
    
        struct relocation_info *rel = &object->drelocs[i];
        rel->r_address += text_size + object->data_slide;
    
    }

}

void aout_init_map (struct aout_object *object) {

    int symbol_i;
    
    add_map_object (object->filename, object->header->a_bss, object->header->a_data, object->header->a_text);
    
    for (symbol_i = 0; symbol_i < object->symtab_count; symbol_i++) {
    
        struct nlist *sym = &object->symtab[symbol_i];
        char *symname = object->strtab + sym->n_strx;
        
        if ((sym->n_type & N_TYPE) == N_UNDF) {
            continue;
        }
        
        if (strcmp (symname, ".text") == 0 || strcmp (symname, ".data") == 0 || strcmp (symname, ".bss") == 0) {
            continue;
        }
        
        if ((sym->n_type & N_TYPE) == N_TEXT) {
            add_map_text_symbol (object->filename, symname, sym->n_value);
        } else if ((sym->n_type & N_TYPE) == N_DATA) {
            add_map_data_symbol (object->filename, symname, sym->n_value);
        } else if ((sym->n_type & N_TYPE) == N_BSS) {
            add_map_bss_symbol (object->filename, symname, sym->n_value);
        }
    
    }

}

void aout_paste (struct aout_object *object) {

    struct aout_exec *header = object->header;
    
    char *obj_text, *obj_data;
    unsigned int obj_text_size, obj_data_size, obj_bss_size;
    
    object->text_slide = text_ptr;
    obj_text = (char *) object->raw + sizeof (*header);
    
    if (state->impure) {
        obj_text_size = header->a_text;
    } else {
    
        if (state->format == LD_FORMAT_I386_PE) {
            obj_text_size = ALIGN_UP (header->a_text, FILE_ALIGNMENT);
        } else {
            obj_text_size = ALIGN_UP (header->a_text, PAGE_SIZE);
        }
    
    }
    
    memcpy ((char *) text + text_ptr, obj_text, header->a_text);
    text_ptr += obj_text_size;
    
    object->data_slide = data_ptr;
    obj_data = (char *) object->raw + sizeof (*header) + header->a_text;
    
    if (state->impure) {
        obj_data_size = header->a_data;
    } else {
    
        if (state->format == LD_FORMAT_I386_PE) {
            obj_data_size = ALIGN_UP (header->a_data, FILE_ALIGNMENT);
        } else {
            obj_data_size = ALIGN_UP (header->a_data, PAGE_SIZE);
        }
    
    }
    
    memcpy ((char *) data + data_ptr, obj_data, header->a_data);
    data_ptr += obj_data_size;
    
    object->bss_slide = bss_ptr;
    
    if (state->impure) {
        obj_bss_size = header->a_bss;
    } else {
    
        if (state->format == LD_FORMAT_I386_PE) {
            obj_bss_size = ALIGN_UP (header->a_bss, FILE_ALIGNMENT);
        } else {
            obj_bss_size = ALIGN_UP (header->a_bss, PAGE_SIZE);
        }
    
    }
    
    bss_ptr += obj_bss_size;

}

void aout_undf_collect (struct aout_object *object) {

    int i, val;
    
    for (i = 0; i < object->symtab_count; i++) {
    
        struct nlist *sym = &object->symtab[i];
        char *symname = object->strtab + sym->n_strx;
        
        if ((sym->n_type & N_TYPE) != N_UNDF || sym->n_value == 0) {
            continue;
        }
        
        if (get_symbol (NULL, symname, 1) != NULL) {
            continue;
        }
        
        sym->n_type = N_BSS | N_EXT;
        val = sym->n_value;
        
        sym->n_value = text_size + data_size + bss_size;
        bss_size += val;
    
    }

}

void aout_free (struct aout_object *object) {

    if (object->raw) {
        free (object->raw);
    }
    
    free (object);

}
