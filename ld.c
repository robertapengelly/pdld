/******************************************************************************
 * @file            ld.c
 *
 * Released to the public domain.
 *
 * Anyone and anything may copy, edit, publish, use, compile, sell and
 * distribute this work and all its parts in any form for any purpose,
 * commercial and non-commercial, without any restrictions, without
 * complying with any conditions and by any means.
 *****************************************************************************/
#include    <stddef.h>

#include    "aout.h"
#include    "ld.h"
#include    "libld.h"

struct ld_state *state;
const char *program_name = 0;

unsigned int text_size = 0;
unsigned int data_size = 0;
unsigned int bss_size = 0;
unsigned int output_size = 0;

void *output = NULL;
void *text = NULL;
void *data = NULL;

size_t header_size = 0;

static unsigned int get_entry (void) {

    struct nlist *symbol;
    
    if (!state->entry || strcmp (state->entry, "") == 0) {
        state->entry = "_start";
    }
    
    if ((symbol = get_symbol (NULL, state->entry, 1)) == NULL) {
    
        if (program_name) {
            fprintf (stderr, "%s: ", program_name);
        }
        
        fprintf (stderr, "warning: cannot find entry symbol %s; defaulting to 00000000\n", state->entry);
        return 0;
    
    }
    
    return symbol->n_value;

}

static void init_map (void) {

    struct object *object;
    int object_i;
    
    for (object_i = 0; object_i < state->nb_objects; object_i++) {
    
        if ((object = state->objects[object_i]) != NULL) {
        
            if (object->init_map) {
                object->init_map (object->data);
            }
        
        }
    
    }

}

static void relocate (void) {

    unsigned int zapdata = (text - output);
    int i, length;
    
    for (i = 0; i < tgr.relocations_count; i++) {
    
        struct relocation_info *r = &tgr.relocations[i];
        unsigned int orig = 0;
        
        if (((r->r_symbolnum >> 24) & 0xff) != N_TEXT) {
            continue;
        }
        
        length = (r->r_symbolnum & (3 << 25)) >> 25;
        
        memcpy (&orig, ((char *) text + r->r_address), length);
        orig += zapdata;
        
        if (state->format == LD_FORMAT_I386_PE) {
        
            unsigned int data_offset = (data - output);
            
            if (orig >= data_offset) {
            
                orig += ALIGN_UP (zapdata, SECTION_ALIGNMENT);
                orig += ALIGN_UP (text_size, SECTION_ALIGNMENT);
                
                orig -= zapdata;
            
            } else {
                orig += ALIGN_UP (zapdata, SECTION_ALIGNMENT);
            }
            
            orig -= zapdata;
        
        }
        
        memcpy ((char *) text + r->r_address, &orig, length);
    
    }
    
    for (i = 0; i < dgr.relocations_count; i++) {
    
        struct relocation_info *r = &dgr.relocations[i];
        unsigned int orig = 0;
        
        if (((r->r_symbolnum >> 24) & 0xff) != N_TEXT) {
            continue;
        }
        
        length = (r->r_symbolnum & (3 << 25)) >> 25;
        
        memcpy (&orig, ((char *) data + r->r_address), length);
        orig += zapdata;
        
        if (state->format == LD_FORMAT_I386_PE) {
        
            unsigned int data_offset = (data - output);
            
            if (orig >= data_offset) {
            
                orig += ALIGN_UP (zapdata, SECTION_ALIGNMENT);
                orig += ALIGN_UP (text_size, SECTION_ALIGNMENT);
                
                orig -= zapdata;
            
            } else {
                orig += ALIGN_UP (zapdata, SECTION_ALIGNMENT);
            }
            
            orig -= zapdata;
        
        }
        
        memcpy ((char *) data + r->r_address, &orig, length);
    
    }

}

void *get_symbol (const char *filename, const char *name, int quiet) {

    struct object *object;
    void *result;
    
    int i;
    
    for (i = 0; i < state->nb_objects; i++) {
    
        if ((object = state->objects[i]) != NULL) {
        
            if (object->get_symbol) {
            
                if ((result = object->get_symbol (object->data, name)) != NULL) {
                    return result;
                }
            
            }
        
        }
    
    }
    
    if (!quiet) {
    
        if (program_name) {
            fprintf (stderr, "%s: ", program_name);
        }
        
        fprintf (stderr, "error: ");
        
        if (filename) {
            fprintf (stderr, "%s: ", filename);
        }
        
        fprintf (stderr, "undefined symbol '%s'\n", name);
    
    }
    
    return NULL;

}

int main (int argc, char **argv) {

    char **pargv = argv;
    int pargc = argc;
    
    struct object *object;
    int i;
    
    int err = 0;
    FILE *ofp = NULL;
    
    if (argc && *argv) {
    
        char *p;
        program_name = *argv;
        
        if ((p = strrchr (program_name, '/'))) {
            program_name = (p + 1);
        }
    
    }
    
    state = xmalloc (sizeof (*state));
    ld_parse_args (&pargc, &pargv, 1);
    
    if (state->nb_files == 0) {
    
        if (program_name) {
            fprintf (stderr, "%s: ", program_name);
        }
        
        fprintf (stderr, "error: no input files provided\n");
        exit (1);
    
    }
    
    if (/*state->format == LD_FORMAT_I386_MSDOS || */state->format == LD_FORMAT_MSDOS) {
    
        if (!state->text_offset_set) {
            state->text_offset = 0x0100;
        }
    
    } else if (state->format == LD_FORMAT_I386_PE) {
    
        if (!state->text_offset_set) {
            state->text_offset = 0x00400000;
        }
    
    }
    
    if (!state->data_offset_set) {
        state->data_offset = state->text_offset;
    }
    
    if (state->format != LD_FORMAT_I386_AOUT) {
    
        if (state->format == LD_FORMAT_I386_PE) {
            state->impure = 0;
        } else {
            state->impure = 1;
        }
    
    }
    
    for (i = 0; i < state->nb_files; i++) {
    
        if (initialize_object (state->files[i])) {
        
            if (program_name) {
                fprintf (stderr, "%s: ", program_name);
            }
            
            fprintf (stderr, "error: failed to initialize '%s'\n", state->files[i]);
            
            err = 1;
            goto out;
        
        }
    
    }
    
    if (state->format == LD_FORMAT_I386_AOUT) {
    
        if ((err = init_aout_object ())) {
        
            if (program_name) {
                fprintf (stderr, "%s: ", program_name);
            }
            
            fprintf (stderr, "error: failed to initialize a.out object\n");
            goto out;
        
        }
    
    } else if (state->format == LD_FORMAT_I386_COFF) {
    
        if ((err = init_coff_object ())) {
        
            if (program_name) {
                fprintf (stderr, "%s: ", program_name);
            }
            
            fprintf (stderr, "error: failed to initialize coff object\n");
            goto out;
        
        }
    
    } else if (state->format == LD_FORMAT_I386_MSDOS) {
    
        if ((err = init_msdos_object ())) {
        
            if (program_name) {
                fprintf (stderr, "%s: ", program_name);
            }
            
            fprintf (stderr, "error: failed to initialize msdos object\n");
            goto out;
        
        }
    
    } else if (state->format == LD_FORMAT_I386_PE) {
    
        if ((err = init_pe_object ())) {
        
            if (program_name) {
                fprintf (stderr, "%s: ", program_name);
            }
            
            fprintf (stderr, "error: failed to initialize pe object\n");
            goto out;
        
        }
    
    } else if (state->format == LD_FORMAT_BINARY || state->format == LD_FORMAT_MSDOS) {
    
        if (state->format == LD_FORMAT_MSDOS) {
        
            text_size = ALIGN_UP (text_size, 16);
            data_size = ALIGN_UP (data_size, 16);
        
        }
        
        output_size = text_size + data_size;
        
        if ((output = malloc (output_size)) == NULL) {
        
            err = 1;
            goto out;
        
        }
        
        memset (output, 0, output_size);
        
        text = (void *) (char *) output;
        data = (void *) ((char *) text + text_size);
    
    } else {
    
        if (program_name) {
            fprintf (stderr, "%s: ", program_name);
        }
        
        fprintf (stderr, "error: unsupported format specified\n");
        
        err = 1;
        goto out;
    
    }
    
    for (i = 0; i < state->nb_objects; i++) {
    
        if ((object = state->objects[i]) != NULL) {
        
            if (object->paste) {
                object->paste (object->data);
            }
        
        }
    
    }
    
    for (i = 0; i < state->nb_objects; i++) {
    
        if ((object = state->objects[i]) != NULL) {
        
            if (object->apply_slides) {
                object->apply_slides (object->data);
            }
        
        }
    
    }
    
    for (i = 0; i < state->nb_objects; i++) {
    
        if ((object = state->objects[i]) != NULL) {
        
            if (object->undf_collect) {
                object->undf_collect (object->data);
            }
        
        }
    
    }
    
    if (!state->impure) {
    
        if (state->format == LD_FORMAT_I386_PE) {
            bss_size = ALIGN_UP (bss_size, FILE_ALIGNMENT);
        } else {
            bss_size = ALIGN_UP (bss_size, PAGE_SIZE);
        }
    
    }
    
    for (i = 0; i < state->nb_objects; i++) {
    
        if ((object = state->objects[i]) != NULL) {
        
            if (object->glue) {
            
                if (object->glue (object->data)) {
                    err = 1;
                }
            
            }
        
        }
    
    }
    
    if (err) {
        goto out;
    }
    
    init_map ();
    
    set_map_sections_size (text_size, data_size, bss_size);
    set_map_sections_start (0, text_size, text_size + data_size);
    
    if ((ofp = fopen (state->outfile, "wb")) == NULL) {
    
        if (program_name) {
            fprintf (stderr, "%s: ", program_name);
        }
        
        fprintf (stderr, "error: failed to open '%s' for writing\n", state->outfile);
        
        err = 1;
        goto out;
    
    }
    
    if (state->format == LD_FORMAT_I386_AOUT) {
    
        unsigned int a_entry = get_entry ();
        
        if ((err = write_aout_object (ofp, a_entry))) {
        
            if (program_name) {
                fprintf (stderr, "%s: ", program_name);
            }
            
            fprintf (stderr, "error: failed to write a.out object\n");
            goto out;
        
        }
    
    } else if (state->format == LD_FORMAT_I386_COFF || state->format == LD_FORMAT_I386_PE) {
    
        unsigned int a_entry = get_entry ();
        relocate ();
        
        if (state->format == LD_FORMAT_I386_COFF) {
        
            if ((err = write_coff_object (ofp, a_entry))) {
            
                if (program_name) {
                    fprintf (stderr, "%s: ", program_name);
                }
                
                fprintf (stderr, "error: failed to write coff object\n");
                goto out;
            
            }
        
        } else {
        
            if ((err = write_pe_object (ofp, a_entry))) {
            
                if (program_name) {
                    fprintf (stderr, "%s: ", program_name);
                }
                
                fprintf (stderr, "error: failed to write pe object\n");
                goto out;
            
            }
        
        }
    
    } else if (state->format == LD_FORMAT_I386_MSDOS) {
    
        relocate ();
        
        if ((err = write_msdos_object (ofp))) {
        
            if (program_name) {
                fprintf (stderr, "%s: ", program_name);
            }
            
            fprintf (stderr, "error: failed to write msdos object\n");
            goto out;
        
        }
    
    } else if (state->format == LD_FORMAT_BINARY || state->format == LD_FORMAT_MSDOS) {
    
        relocate ();
        
        if (fwrite ((char *) output, text_size + data_size, 1, ofp) != 1) {
        
            if (program_name) {
                fprintf (stderr, "%s: ", program_name);
            }
            
            fprintf (stderr, "error: failed to write data to '%s'\n", state->outfile);
            
            err = 1;
            goto out;
        
        }
    
    }
    
    generate_map ();

out:

    for (i = 0; i < state->nb_objects; i++) {
    
        if ((object = state->objects[i]) != NULL) {
        
            if (object->free) {
                object->free (object->data);
            }
            
            free (object);
        
        }
    
    }
    
    if (output != NULL) {
        free (output);
    }
    
    if (ofp != NULL) {
        fclose (ofp);
    }
    
    if (err > 0 && state->outfile) {
        remove (state->outfile);
    }
    
    return err;

}
