/******************************************************************************
 * @file            table.c
 *
 * Released to the public domain.
 *
 * Anyone and anything may copy, edit, publish, use, compile, sell and
 * distribute this work and all its parts in any form for any purpose,
 * commercial and non-commercial, without any restrictions, without
 * complying with any conditions and by any means.
 *****************************************************************************/
#include    <stddef.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>

#include    "table.h"

static struct table name_table;

static struct name *find_name_table (const char *chars, size_t bytes, unsigned int hash) {

    struct table *table = &name_table;
    unsigned int index;
    
    if (table->count == 0) {
        return NULL;
    }
    
    for (index = hash % table->capacity; ; index = (index + 1) % table->capacity) {
    
        struct table_entry *entry = &table->entries[index];
        struct name *key = entry->key;
        
        if (key == NULL) {
        
            if (entry->value == NULL) {
                return NULL;
            }
        
        } else {
        
            if (key->bytes == bytes && key->hash == hash && memcmp (key->chars, chars, bytes) == 0) {
                return key;
            }
        
        }
    
    }

}

static struct table_entry *find_entry (struct table_entry *entries, int capacity, struct name *key) {

    struct table_entry *tombstone = NULL;
    unsigned int index;
    
    for (index = key->hash % capacity; ; index = (index + 1) % capacity) {
    
        struct table_entry *entry = &entries[index];
        
        if (entry->key == NULL) {
        
            if (entry->value == NULL) {
            
                if (tombstone != NULL) {
                    return tombstone;
                }
                
                return entry;
            
            }
            
            if (tombstone == NULL) {
                tombstone = entry;
            }
        
        } else {
        
            if (entry->key == key) {
                return entry;
            }
        
        }
    
    }

}

static int adjust_capacity (struct table *table, int new_capacity) {

    struct table_entry *new_entries, *old_entries;
    int i, new_count, old_capacity;
    
    if ((new_entries = malloc (sizeof (*new_entries) * new_capacity)) == NULL) {
        return -1;
    }
    
    for (i = 0; i < new_capacity; ++i) {
    
        struct table_entry *entry = &new_entries[i];
        entry->key = NULL;
        entry->value = NULL;
    
    }
    
    new_count = 0;
    
    old_entries = table->entries;
    old_capacity = table->capacity;
    
    for (i = 0; i < old_capacity; ++i) {
    
        struct table_entry *entry = &old_entries[i];
        struct table_entry *dest;
        
        if (entry->key == NULL) {
            continue;
        }
        
        dest = find_entry (new_entries, new_capacity, entry->key);
        dest->key = entry->key;
        dest->value = entry->value;
        
        ++new_count;
    
    }
    
    free (old_entries);
    
    table->entries = new_entries;
    table->capacity = new_capacity;
    table->count = table->used = new_count;
    
    return 0;

}

static unsigned int hash_string (const char *key, size_t length) {

    const unsigned char *u = (const unsigned char *) key;
    unsigned int hash = 0;
    
    size_t i;
    
    for (i = 0; i < length; ++i) {
        hash = u[i] + (hash << 6) + (hash << 16) - hash;
    }
    
    return hash;

}

struct name *alloc_name (const char *begin, const char *end, int make_copy) {

    struct name *name;
    unsigned int hash;
    
    size_t bytes;
    
    if (end != NULL) {
        bytes = (size_t) (end - begin);
    } else {
        bytes = strlen (begin);
    }
    
    hash = hash_string (begin, bytes);
    
    if ((name = find_name_table (begin, bytes, hash)) == NULL) {
    
        if (make_copy) {
        
            char *new_str;
            
            if ((new_str = malloc (bytes + 1)) == NULL) {
                return NULL;
            }
            
            memcpy (new_str, begin, bytes);
            new_str[bytes] = '\0';
            
            begin = new_str;
        
        }
        
        if ((name = malloc (sizeof (*name))) == NULL) {
            return NULL;
        }
        
        name->bytes = bytes;
        name->chars = begin;
        name->hash = hash;
        
        if (table_put (&name_table, name, name)) {
            return NULL;
        }
    
    }
    
    return name;

}

int table_put (struct table *table, struct name *key, void *value) {

    const int MIN_CAPACITY = 15;
    
    struct table_entry *entry;
    int is_new_key;
    
    if (table->used >= table->capacity / 2) {
    
        int capacity = table->capacity * 2 - 1;
        
        if (capacity < MIN_CAPACITY) {
            capacity = MIN_CAPACITY;
        }
        
        if (adjust_capacity (table, capacity) == -1) {
            return 2;
        }
    
    }
    
    entry = find_entry (table->entries, table->capacity, key);
    is_new_key = (entry->key == NULL);
    
    if (is_new_key) {
    
        ++table->count;
        
        if (entry->value == NULL) {
            ++table->used;
        }
    
    }
    
    entry->key = key;
    entry->value = value;
    
    return 0;

}

void *table_get (struct table *table, struct name *key) {

    struct table_entry *entry;
    
    if (table->count == 0) {
        return NULL;
    }
    
    entry = find_entry (table->entries, table->capacity, key);
    
    if (entry->key == NULL) {
        return NULL;
    }
    
    return entry->value;

}
