This package was originally written by Mintsuki and can be obtained from:

https://github.com/mintsuki/pdld

Paul Edwards did some bug fixes.

Robert Pengelly rewrote the utility and added a few more
command line options and can be obtained from:

https://gitlab.com/robertapengelly/pdld

All code is public domain.

The makefiles are written for use under Linux, macOS and Windows
using GCC and MINGW and will need to be modified to use other
tools and libraries.