/******************************************************************************
 * @file            pe.h
 *
 * Released to the public domain.
 *
 * Anyone and anything may copy, edit, publish, use, compile, sell and
 * distribute this work and all its parts in any form for any purpose,
 * commercial and non-commercial, without any restrictions, without
 * complying with any conditions and by any means.
 *****************************************************************************/
struct msdos_header {

    unsigned char   e_magic[2];         /* Magic number                         */
    unsigned short  e_cblp;             /* Bytes on last page of file           */
    unsigned short  e_cp;               /* Pages in file                        */
    unsigned short  e_crlc;             /* Relocations                          */
    unsigned short  e_cparhdr;          /* Size of header in paragraphs         */
    unsigned short  e_minalloc;         /* Minimum extra paragraphs needed      */
    unsigned short  e_maxalloc;         /* Maximum extra paragraphs needed      */
    unsigned short  e_ss;               /* Initial (relative) SS value          */
    unsigned short  e_sp;               /* Initial SP value                     */
    unsigned short  e_csum;             /* Checksum                             */
    unsigned short  e_ip;               /* Initial IP value                     */
    unsigned short  e_cs;               /* Initial (relative) CS value          */
    unsigned short  e_lfarlc;           /* File address of relocation table     */
    unsigned short  e_ovno;             /* Overlay number                       */
    unsigned short  e_res[4];           /* Reserved words                       */
    unsigned short  e_oemid;            /* OEM identifier (for e_oeminfo)       */
    unsigned short  e_oeminfo;          /* OEM information; e_oemid specific    */
    unsigned short  e_res2[10];         /* Reserved words                       */
    unsigned int    e_lfanew;           /* File address of new exe header       */

};

struct pe_header {

    unsigned char Signature[4];
    
    unsigned short Machine;
    unsigned short NumberOfSections;
    
    unsigned int TimeDateStamp;
    unsigned int PointerToSymbolTable;
    unsigned int NumberOfSymbols;
    
    unsigned short SizeOfOptionalHeader;
    unsigned short Characteristics;

};

#define     IMAGE_FILE_MACHINE_I386                         0x014C

#define     IMAGE_FILE_RELOCS_STRIPPED                      0x0001
#define     IMAGE_FILE_EXECUTABLE_IMAGE                     0x0002
#define     IMAGE_FILE_LINE_NUMS_STRIPPED                   0x0004
#define     IMAGE_FILE_LOCAL_SYMS_STRIPPED                  0x0008
#define     IMAGE_FILE_32BIT_MACHINE                        0x0100
#define     IMAGE_FILE_DEBUG_STRIPPED                       0x0200

struct pe_optional_header {

    unsigned short Magic;
    
    unsigned char MajorLinkerVersion;
    unsigned char MinorLinkerVersion;
    
    unsigned int SizeOfCode;
    unsigned int SizeOfInitializedData;
    unsigned int SizeOfUninitializedData;
    
    unsigned int AddressOfEntryPoint;
    
    unsigned int BaseOfCode;
    unsigned int BaseOfData;
    
    
    unsigned int ImageBase;
    unsigned int SectionAlignment;
    unsigned int FileAlignment;
    
    unsigned short MajorOperatingSystemVersion;
    unsigned short MinorOperatingSystemVersion;
    unsigned short MajorImageVersion;
    unsigned short MinorImageVersion;
    unsigned short MajorSubsystemVersion;
    unsigned short MinorSubsystemVersion;
    
    unsigned int Win32VersionValue;;
    unsigned int SizeOfImage;
    unsigned int SizeOfHeaders;
    unsigned int Checksum;
    
    unsigned short Subsystem;
    unsigned short DllCharacteristics;
    
    unsigned int SizeOfStackReserved;
    unsigned int SizeOfStackCommit;
    unsigned int SizeOfHeapReserved;
    unsigned int SizeOfHeapCommit;
    
    unsigned int LoaderFlags;
    unsigned int NumberOfRvaAndSizes;
    
    
    /*unsigned char Reserved[128];*/
    unsigned int Reserved[32];

};

#define     IMAGE_FILE_MAGIC_I386                           0x010B

struct section_table_entry {

    char Name[8];
    
    unsigned int VirtualSize;
    unsigned int VirtualAddress;
    
    unsigned int SizeOfRawData;
    unsigned int PointerToRawData;
    unsigned int PointerToRelocations;
    unsigned int PointerToLinenumbers;
    
    unsigned short NumberOfRelocations;
    unsigned short NumberOfLinenumbers;
    
    unsigned int Characteristics;

};

#define     IMAGE_SCN_CNT_CODE                              0x00000020
#define     IMAGE_SCN_CNT_INITIALIZED_DATA                  0x00000040
#define     IMAGE_SCN_CNT_UNINITIALIZED_DATA                0x00000080
#define     IMAGE_SCN_ALIGN_4BYTES                          0x00300000
#define     IMAGE_SCN_MEM_EXECUTE                           0x20000000
#define     IMAGE_SCN_MEM_READ                              0x40000000
#define     IMAGE_SCN_MEM_WRITE                             0x80000000

struct relocation_entry {

    unsigned int VirtualAddress;
    unsigned int SymbolTableIndex;
    
    unsigned short Type;

};

#define     RELOCATION_ENTRY_SIZE                           10

#define     IMAGE_REL_I386_ABSOLUTE                         0x0000
#define     IMAGE_REL_I386_DIR32                            0x0006
#define     IMAGE_REL_I386_DIR32NB                          0x0007
#define     IMAGE_REL_I386_REL32                            0x0014

struct symbol_table_entry {

    char Name[8];
    unsigned int Value;
    
    signed short SectionNumber;
    unsigned short Type;
    
    unsigned char StorageClass;
    unsigned char NumberOfAuxSymbols;

};

#define     SYMBOL_TABLE_ENTRY_SIZE                         18

#define     IMAGE_SYM_UNDEFINED                             0
#define     IMAGE_SYM_ABSOLUTE                              -1
#define     IMAGE_SYM_DEBUG                                 -2

#define     IMAGE_SYM_TYPE_NULL                             0
#define     IMAGE_SYM_DTYPE_NULL                            0

#define     IMAGE_SYM_CLASS_EXTERNAL                        2
#define     IMAGE_SYM_CLASS_STATIC                          3
#define     IMAGE_SYM_CLASS_FILE                            103
