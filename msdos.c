/******************************************************************************
 * @file            msdos.c
 *
 * Released to the public domain.
 *
 * Anyone and anything may copy, edit, publish, use, compile, sell and
 * distribute this work and all its parts in any form for any purpose,
 * commercial and non-commercial, without any restrictions, without
 * complying with any conditions and by any means.
 *****************************************************************************/
#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>

#include    "msdos.h"
#include    "ld.h"

static struct msdos_header *header;

int init_msdos_object (void) {

    text_size = ALIGN_UP (text_size, 16);
    data_size = ALIGN_UP (data_size, 16);
    
    header_size = ALIGN_UP (sizeof (*header), 16);
    output_size = header_size + text_size + data_size;
    
    if ((output = malloc (output_size)) == NULL) {
        return 2;
    }
    
    memset (output, 0, output_size);
    header = output;
    
    text = (void *) ((char *) output + header_size);
    data = (void *) ((char *) text + text_size);
    
    return 0;

}

int write_msdos_object (FILE *ofp) {

    unsigned short ibss_addr = (data - output) + data_size;
    unsigned short ibss_size = bss_size;
    
    unsigned short stack_addr = ibss_addr + ibss_size;
    unsigned short stack_size = ALIGN_UP (stack_addr, PAGE_SIZE);
    
    header->e_magic[0] = 'M';
    header->e_magic[1] = 'Z';
    
    header->e_cblp = ibss_addr % FILE_ALIGNMENT;
    header->e_cp = ALIGN_UP (ibss_addr, FILE_ALIGNMENT) / FILE_ALIGNMENT;
    
    header->e_cparhdr = header_size / 16;
    
    header->e_minalloc = ALIGN_UP (ibss_size + stack_size, 16) / 16;
    header->e_maxalloc = 0xFFFF;
    
    header->e_ss = stack_addr / 16;
    header->e_sp = stack_addr % 16 + stack_size;
    
    header->e_lfarlc = sizeof (*header);
    
    if (fwrite ((char *) output, output_size, 1, ofp) != 1) {
    
        if (program_name) {
            fprintf (stderr, "%s: ", program_name);
        }
        
        fprintf (stderr, "error: failed to write data to '%s'\n", state->outfile);
        return 1;
    
    }
    
    return 0;

}
