/******************************************************************************
 * @file            coff.c
 *
 * Released to the public domain.
 *
 * Anyone and anything may copy, edit, publish, use, compile, sell and
 * distribute this work and all its parts in any form for any purpose,
 * commercial and non-commercial, without any restrictions, without
 * complying with any conditions and by any means.
 *****************************************************************************/
#include    <stddef.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>
#include    <time.h>

#include    "coff.h"
#include    "ld.h"

static struct coff_header *header;
static struct coff_optional_header *optional_header;

int init_coff_object (void) {

    text_size = ALIGN_UP (text_size, 16);
    data_size = ALIGN_UP (data_size, 16);
    
    header_size = sizeof (*header) + sizeof (*optional_header);
    output_size = header_size + text_size + data_size;
    
    if ((output = malloc (output_size)) == NULL) {
        return 2;
    }
    
    memset (output, 0, output_size);
    
    header = output;
    optional_header = (void *) ((char *) output + sizeof (*header));
    
    text = (void *) ((char *) output + header_size);
    data = (void *) ((char *) text + text_size);
    
    return 0;

}

int write_coff_object (FILE *ofp, unsigned int entry) {

    header->Machine = IMAGE_FILE_MACHINE_I386;
    header->TimeDateStamp = time (0);
    header->SizeOfOptionalHeader = sizeof (*optional_header);
    
    header->Characteristics |= IMAGE_FILE_RELOCS_STRIPPED;
    header->Characteristics |= IMAGE_FILE_EXECUTABLE_IMAGE;
    header->Characteristics |= IMAGE_FILE_LINE_NUMS_STRIPPED;
    header->Characteristics |= IMAGE_FILE_32BIT_MACHINE;
    
    optional_header->Magic = IMAGE_FILE_MAGIC_I386;
    optional_header->SizeOfCode = text_size;
    optional_header->SizeOfInitializedData = data_size;
    optional_header->SizeOfUninitializedData = bss_size;
    optional_header->AddressOfEntryPoint = (text - output) + entry;
    optional_header->BaseOfCode = (text - output);
    optional_header->BaseOfData = (data - output);
    
    if (fwrite ((char *) output, output_size, 1, ofp) != 1) {
    
        if (program_name) {
            fprintf (stderr, "%s: ", program_name);
        }
        
        fprintf (stderr, "error: failed to write data to '%s'\n", state->outfile);
        return 1;
    
    }
    
    return 0;

}
