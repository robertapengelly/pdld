/******************************************************************************
 * @file            libld.c
 *
 * Released to the public domain.
 *
 * Anyone and anything may copy, edit, publish, use, compile, sell and
 * distribute this work and all its parts in any form for any purpose,
 * commercial and non-commercial, without any restrictions, without
 * complying with any conditions and by any means.
 *****************************************************************************/
#include    <ctype.h>
#include    <errno.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>

#include    "ld.h"
#include    "libld.h"
#include    "ldpp.h"

struct ld_option {

    const char *name;
    unsigned short index, flags;

};

enum {

    LD_OPTION_IGNORED = 0,
    LD_OPTION_DATA,
    LD_OPTION_ENTRY,
    LD_OPTION_FORMAT,
    LD_OPTION_HELP,
    LD_OPTION_IMPURE,
    LD_OPTION_MAP,
    LD_OPTION_MAPFILE,
    LD_OPTION_NOSTDLIB,
    LD_OPTION_OUTFILE,
    LD_OPTION_STRIP,
    LD_OPTION_TEXT

};

#define     LD_OPTION_NO_ARG            0x0000
#define     LD_OPTION_HAS_ARG           0x0001

static const struct ld_option ld_options[] = {

    { "e",          LD_OPTION_ENTRY,        LD_OPTION_HAS_ARG           },
    { "o",          LD_OPTION_OUTFILE,      LD_OPTION_HAS_ARG           },
    { "M",          LD_OPTION_MAP,          LD_OPTION_NO_ARG            },
    { "N",          LD_OPTION_IMPURE,       LD_OPTION_NO_ARG            },
    { "s",          LD_OPTION_STRIP,        LD_OPTION_NO_ARG            },
    
    { "Map",        LD_OPTION_MAPFILE,      LD_OPTION_HAS_ARG           },
    { "nostdlib",   LD_OPTION_NOSTDLIB,     LD_OPTION_NO_ARG            },
    
    { "Tdata",      LD_OPTION_DATA,         LD_OPTION_HAS_ARG           },
    { "Ttext",      LD_OPTION_TEXT,         LD_OPTION_HAS_ARG           },
    
    { "-oformat",   LD_OPTION_FORMAT,       LD_OPTION_HAS_ARG           },
    { "-help",      LD_OPTION_HELP,         LD_OPTION_NO_ARG            },
    
    { NULL,         0,                      0                           }

};

static int _strstart (const char *val, const char **str);

static void _convert_to_lower (const char *in, char *out);
static void _print_help (const char *name);

static int _strstart (const char *val, const char **str) {

    const char *p = *str;;
    const char *q = val;
    
    while (*q) {
    
        if (*p != *q) {
            return 0;
        }
        
        p++;
        q++;
    
    }
    
    *str = p;
    return 1;

}

static void _convert_to_lower (const char *in, char *out) {

    while (*in) {
        *out++ = tolower (*in++);
    }

}

static void _print_help (const char *name) {

    char *p;
    
    if ((p = strrchr (name, '/'))) {
        name = (p + 1);
    }
    
    fprintf (stderr, "Usage: %s [options] file...\n\n", name);
    fprintf (stderr, "Options:\n\n");
    fprintf (stderr, "    -N                    Do not page align data\n");
    fprintf (stderr, "    -o FILE               Set output file name (default a.out)\n");
    fprintf (stderr, "    -s                    Strip all\n");
    fprintf (stderr, "    -e ADDRESS            Set start address\n");
    fprintf (stderr, "    -M                    Print map file on standard out\n");
    fprintf (stderr, "    -Map FILE             Write a map file\n");
    fprintf (stderr, "    -Tdata ADDRESS        Set address of .data section\n");
    fprintf (stderr, "    -Ttext ADDRESS        Set address of .text section\n");
    fprintf (stderr, "    --oformat FORMAT      Specify the format of output file (default a.out-i386)\n");
    fprintf (stderr, "                              Supported formats are:\n");
    fprintf (stderr, "                                  a.out-i386, coff-i386, msdos-i386\n");
    fprintf (stderr, "                                  binary, msdos\n");
    fprintf (stderr, "    --help                Print this help information\n");
    
    fprintf (stderr, "\n");
    exit (EXIT_SUCCESS);

}

char *xstrdup (const char *str) {

    char *ptr = xmalloc (strlen (str) + 1);
    strcpy (ptr, str);
    
    return ptr;

}

void ld_parse_args (int *pargc, char ***pargv, int optind) {

    char **argv = *pargv;
    int argc = *pargc;
    
    const struct ld_option *popt;
    const char *optarg, *r;
    
    char *temp;
    long long conversion;
    
    if (argc == optind) {
        _print_help (argv[0]);
    }
    
    while (optind < argc) {
    
        r = argv[optind++];
        
        if (r[0] != '-' || r[1] == '\0') {
        
            dynarray_add (&state->files, &state->nb_files, xstrdup (r));
            continue;
        
        }
        
        /* find option in table. */
        for (popt = ld_options; popt; ++popt) {
        
            const char *p1 = popt->name;
            const char *r1 = r + 1;
            
            if (p1 == NULL) {
            
                if (program_name) {
                    fprintf (stderr, "%s: ", program_name);
                }
                
                fprintf (stderr, "error: invalid option -- '%s'\n", r);
                exit (EXIT_FAILURE);
            
            }
            
            if (!_strstart (p1, &r1)) {
                continue;
            }
            
            optarg = r1;
            
            if (popt->flags & LD_OPTION_HAS_ARG) {
            
                if (*r1 == '\0') {
                
                    if (optind >= argc) {
                    
                        if (program_name) {
                            fprintf (stderr, "%s: ", program_name);
                        }
                        
                        fprintf (stderr, "error: argument to '%s' is missing\n", r);
                        exit (EXIT_FAILURE);
                    
                    }
                    
                    optarg = argv[optind++];
                
                }
            
            } else if (*r1 != '\0') {
                continue;
            }
            
            break;
        
        }
        
        switch (popt->index) {
        
            case LD_OPTION_DATA:
            
                temp = xmalloc (strlen (optarg) + 1);
                _convert_to_lower (optarg, temp);
                
                if (*optarg == '=') {
                    optarg++;
                }
                
                if (!optarg || strcmp (optarg, "") == 0) {
                
                    if (program_name) {
                        fprintf (stderr, "%s: ", program_name);
                    }
                    
                    fprintf (stderr, "error: missing argument for -Tdata\n");
                    exit (EXIT_FAILURE);
                
                }
                
                errno = 0;
                conversion = strtol (optarg, &temp, 0);
                
                if (!*optarg || isspace ((int) *optarg) || errno || *temp) {
                
                    if (program_name) {
                        fprintf (stderr, "%s: ", program_name);
                    }
                    
                    fprintf (stderr, "error: invalid argument '%s' for -Tdata\n", optarg);
                    exit (EXIT_FAILURE);
                
                }
                
                if (conversion < 0 || conversion > 0xffff) {
                
                    if (program_name) {
                        fprintf (stderr, "%s: ", program_name);
                    }
                    
                    fprintf (stderr, "error: section address must be between 0 and %u\n", 0xffff);
                    exit (EXIT_FAILURE);
                
                }
                
                state->data_offset = conversion;
                state->data_offset_set = 1;
                
                break;
            
            case LD_OPTION_ENTRY:
            
                state->entry = xstrdup (optarg);
                break;
            
            case LD_OPTION_FORMAT:
            
                temp = xmalloc (strlen (optarg) + 1);
                _convert_to_lower (optarg, temp);
                
                if (strcmp (temp, "a.out-i386") == 0) {
                    state->format = LD_FORMAT_I386_AOUT;
                } else if (strcmp (temp, "coff-i386") == 0) {
                    state->format = LD_FORMAT_I386_COFF;
                } else if (strcmp (temp, "msdos-i386") == 0) {
                    state->format = LD_FORMAT_I386_MSDOS;
                } else if (strcmp (temp, "pe-i386") == 0) {
                    state->format = LD_FORMAT_I386_PE;
                } else if (strcmp (temp, "binary") == 0) {
                    state->format = LD_FORMAT_BINARY;
                } else if (strcmp (temp, "msdos") == 0) {
                    state->format = LD_FORMAT_MSDOS;
                } else {
                
                    if (program_name) {
                        fprintf (stderr, "%s: ", program_name);
                    }
                    
                    fprintf (stderr, "error: unsuppored format '%s' specified\n", temp);
                    
                    free (temp);
                    exit (EXIT_FAILURE);
                
                }
                
                free (temp);
                break;
            
            case LD_OPTION_HELP:
            
                _print_help (argv[0]);
                break;
            
            case LD_OPTION_IMPURE:
            
                state->impure = 1;
                break;
            
            case LD_OPTION_MAP:
            
                if (state->mapfile) {
                
                    if (program_name) {
                        fprintf (stderr, "%s: ", program_name);
                    }
                    
                    fprintf (stderr, "error: multiple map files provided\n");
                    exit (EXIT_FAILURE);
                
                }
                
                state->mapfile = "";
                break;
            
            case LD_OPTION_MAPFILE:
            
                if (state->mapfile) {
                
                    if (program_name) {
                        fprintf (stderr, "%s: ", program_name);
                    }
                    
                    fprintf (stderr, "error: multiple map files provided\n");
                    exit (EXIT_FAILURE);
                
                }
                
                state->mapfile = xstrdup (optarg);
                break;
            
            case LD_OPTION_NOSTDLIB:
            
                state->nostdlib = 1;
                break;
            
            case LD_OPTION_OUTFILE:
            
                if (state->outfile) {
                
                    if (program_name) {
                        fprintf (stderr, "%s: ", program_name);
                    }
                    
                    fprintf (stderr, "error: multiple output files provided\n");
                    exit (EXIT_FAILURE);
                
                }
                
                state->outfile = xstrdup (optarg);
                break;
            
            case LD_OPTION_STRIP:
            
                state->strip_all = 1;
                break;
            
            case LD_OPTION_TEXT:
            
                temp = xmalloc (strlen (optarg) + 1);
                _convert_to_lower (optarg, temp);
                
                if (*optarg == '=') {
                    optarg++;
                }
                
                if (!optarg || strcmp (optarg, "") == 0) {
                
                    if (program_name) {
                        fprintf (stderr, "%s: ", program_name);
                    }
                    
                    fprintf (stderr, "error: missing argument for -Ttext\n");
                    exit (EXIT_FAILURE);
                
                }
                
                errno = 0;
                conversion = strtol (optarg, &temp, 0);
                
                if (!*optarg || isspace ((int) *optarg) || errno || *temp) {
                
                    if (program_name) {
                        fprintf (stderr, "%s: ", program_name);
                    }
                    
                    fprintf (stderr, "error: invalid argument '%s' for -Ttext\n", optarg);
                    exit (EXIT_FAILURE);
                
                }
                
                if (conversion < 0 || conversion > 0xffff) {
                
                    if (program_name) {
                        fprintf (stderr, "%s: ", program_name);
                    }
                    
                    fprintf (stderr, "error: section address must be between 0 and %u\n", 0xffff);
                    exit (EXIT_FAILURE);
                
                }
                
                state->text_offset = conversion;
                state->text_offset_set = 1;
                
                break;
            
            default:
            
                if (program_name) {
                    fprintf (stderr, "%s: ", program_name);
                }
                
                fprintf (stderr, "error: unsupported option '%s'\n", r);
                exit (EXIT_FAILURE);
        
        }
    
    }
    
    if (!state->outfile) { state->outfile = "a.out"; }

}

void *xmalloc (unsigned long size) {

    void *ptr = malloc (size);
    
    if (!ptr && size) {
    
        if (program_name) {
            fprintf (stderr, "%s: ", program_name);
        }
        
        fprintf (stderr, "error: memory full (malloc)\n");
        exit (EXIT_FAILURE);
    
    }
    
    memset (ptr, 0, size);
    return ptr;

}

void *xrealloc (void *ptr, unsigned long size) {

    void *new_ptr = realloc (ptr, size);
    
    if (!new_ptr && size) {
    
        if (program_name) {
            fprintf (stderr, "%s: ", program_name);
        }
        
        fprintf (stderr, "error: memory full (realloc)\n");
        exit (EXIT_FAILURE);
    
    }
    
    return new_ptr;

}

void dynarray_add (void *ptab, int *nb_ptr, void *data) {

    int nb, nb_alloc;
    void **pp;
    
    nb = *nb_ptr;
    pp = *(void ***) ptab;
    
    if ((nb & (nb - 1)) == 0) {
    
        if (!nb) {
            nb_alloc = 1;
        } else {
            nb_alloc = nb * 2;
        }
        
        pp = xrealloc (pp, nb_alloc * sizeof (void *));
        *(void ***) ptab = pp;
    
    }
    
    pp[nb++] = data;
    *nb_ptr = nb;

}
